# Trivia-app

This program is a solution to Task02 of Javascript at Experis Academy Denmark

This program is a singleplayer version of a trivia game, where you have to answer 10 questions

Every time you answer a question correctly, you get 100 points

## Installation
Clone this repository, by running the following git commands: 

```bash
cd existing_folder
git init
git remote add origin git@gitlab.com:AndreasKjelstrup/trivia-app.git
git add .
```

After this is done, run the following commands in a terminal:

```
cd trivia-app
npm run serve
```

This will start up the server, and then navigate to your chosen location where the app is running at


### Maintainers 
Andreas Kjelstrup

### License
Copyright 2020, Andreas Kjelstrup